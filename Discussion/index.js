// Getting all post

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Checking the status of the request
fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));

// https://jsonplaceholder.typicode.com/

/*==========================================================================*/

// ***Async and Await***

async function fetchData() {
        let result = await fetch('jsonplaceholder.typicode.com/posts')
        console.log(result);

        /* Async can stand alone even without Await, While Await cant work without Async*/  
    console.log(typeof result);

    console.log(result.body);

    let json = await result.json();
    console.log(json)
}
fetchData()

// ***GET A SPECIFIC POSTS***
	// (/posts/:id)
    fetch('https://jsonplaceholder.typicode.com/posts/64')
    .then((response) => response.json())
    .then((json) => console.log(json))
    
// ***CREATE A POST***
    
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            title: 'New Post',
            body: 'I am a new post',
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json));
    
// ***UPDATING A POST***
    
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'PUT',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            id: 1,
            title: 'Updated Post',
            body: 'This is an updated post',
            userId: 1
        })
    
    })
    .then((response) => response.json())
    .then((json) => console.log(json));
    
// ***UPDATING POST USING PATCH METHOD***
    
    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PATCH',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            title: 'Correct post'
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json));
    

// ***DELETE A POST***

    fetch('https://jsonplaceholder.typicode.com/todos/12', {
        method: 'DELETE'
    })

// ***FILTERING POSTS***

    fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=10') //with multiple filtering post
    .then((response) => response.json())
    .then((json) => console.log(json))

// ***RETRIEVING SPECIFIC COMMENTS***

    fetch('https://jsonplaceholder.typicode.com/posts/1/comments') 
    .then((response) => response.json())
    .then((json) => console.log(json))
