/* 
1. In the S29 folder, create an activity and images folder and an index.html and a script.js file inside of it.
2. In images folder store your output screenshots

3. Link the script.js file to the index.html file

4. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API. 
        https://jsonplaceholder.typicode.com/todos

5. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

6. Create a fetch request using the GET method that will retrieve a single to do list items from JSON Placeholder API.

    a. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item


7. Create a fetch request using the POST method that will create to do list items using JSON Placeholder API

8. Create a fetch request using the PUT method that will update a to do list items using the JSON Placeholder API.

9.Update to do list items by changing the data structure to contain the following properties:
    a. Title
    b. Description
    c. Status
    d. Date Completed
    e. User ID

10. Creat a fetch request using the PATCH method that will update a to do list items using the JSON Placeholder API.

11. Update a to do list item by changing the status to complete and add a date when the status was changed.
12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API
13. Create a request via Postman to retrieve all the to do list items.
    a. GET HTTP method
    b. https://jsonplaceholder.typicode.com/todos URI endpoint
    c. Save this request as 'get all to do list items'


 14. Create a request via POSTMAN to retrieve an individual to do list item.
    a. GET HTTP method
    b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    c. Save this request as 'get to do list items'

15. Create a service request via Postman to create a to do list item.
    a. POST HTTP method
    b. https://jsonplaceholder.typicode.com/todos URI endpoint
    c. Save this request as 'create to do list items'

16. Create a service request via Postman to update a to do list item.
    a. PUT HTTP method
    b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    c. Save this request as 'update to do list items PUT'
    d. Update the to do list item to mirror the data structure used in the PUT fetch

17. Create a service request via Postman to update a to do list item.
    a. PATCH HTTP method
    b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    c. Save this request as 'create to do list items'
    d. Update the to do list item to mirror the data structure of the PATCH fetch request

18. Create a  request via Postman to DELETE a to do list item.
    a. DELETE HTTP method
    b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
    c. Save this request as 'Delete to do list item'

19. EXPORT the POSTMAN collection and save it in the activity folder.
20. Create a git repository named S29
21. Initialize a local git repository, add the remote link and push to git with the commit message of "Add activity code"
22. Add the link in Boodle 

*/

// SOL'N:

// ITEM 4.
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => console.log(json));

// Item 5-6
let titles = []
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => titles = json.map(obj => obj.title));


// Item 7-8
fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
        id: 1,
        title: 'create to do list items',
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
        description: "Create a fetch request using the PUT method",
        id: 1,
        status: 'Pending',
        title: 'Updated to do list item',
        userId: 1
    })

})
.then((response) => response.json())
.then((json) => console.log(json));

// Item 9-11
    fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            title: 'delectus aut autem',
            status: "Completed",
            dateCompleted: "1/26/2022",
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

    fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            title: 'Updated to do list item',
            description: "To update the my to do list with a different data structure",
            status: "Pending",
            dateCompleted: "Pending",
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

    // item 12

    fetch('https://jsonplaceholder.typicode.com/todos/10', {
        method: 'DELETE'
    })
